package uk.me.westmacott.peter.tankGameServer;

import java.util.Comparator;

public class IpAddressComparator implements Comparator<String> {

	public int compare(String address1, String address2) {
		
		int score1 = 0;
		int score2 = 0;

		score1 += address1.contains(":") ? 1 : 0; // indicates IPv6 alternative
		score2 += address2.contains(":") ? 1 : 0;
		
		score1 += address1.startsWith("127") ? 1 : 0; // indicates machine-local address
		score2 += address2.startsWith("127") ? 1 : 0;
		
		return score1 - score2;
	}
	
}
