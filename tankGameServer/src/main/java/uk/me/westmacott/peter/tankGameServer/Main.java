package uk.me.westmacott.peter.tankGameServer;

import java.awt.Desktop;
import java.net.SocketException;
import java.net.URI;
import java.util.Collections;
import java.util.List;

public class Main {

	private static final int PORT = 80;  
	
	public static void main(String[] args) throws Exception {
		new TankGameServer(PORT).start();
		System.out.println(getAccessAddress());
		openWebPage();
	}

	private static String getAccessAddress() throws SocketException {
		List<String> addresses = NetworkAware.getIpAddresses();
		Collections.sort(addresses, new IpAddressComparator());
		return "http://" + addresses.get(0) + ":" + PORT + "/tanks.html";
	}
	
	private static void openWebPage() throws Exception {
		Desktop desktop;
		if (!Desktop.isDesktopSupported() || !(desktop = Desktop.getDesktop()).isSupported(Desktop.Action.BROWSE)) {
			return;
		}
		desktop.browse(new URI(getAccessAddress()));
	}

}
