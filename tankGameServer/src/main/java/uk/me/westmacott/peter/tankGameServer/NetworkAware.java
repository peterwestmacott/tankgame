package uk.me.westmacott.peter.tankGameServer;


import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NetworkAware {
	
	public static List<String> getIpAddresses() throws SocketException {
    	List<String> addresses = new ArrayList<String>();
        for (NetworkInterface net : Collections.list(NetworkInterface.getNetworkInterfaces())) {
        	for (InetAddress address : Collections.list(net.getInetAddresses())) {
        		addresses.add(address.getHostAddress());
        	}
        }
		return addresses;
    }

}