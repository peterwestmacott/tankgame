package uk.me.westmacott.peter.tankGameServer;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketHandler;

public class TankGameServer extends Server {

	private static final String RESOURCE_PATH = ".";

	public TankGameServer(int port) {
		super(port);
		setHandler(new TankHandlerList());
	}

	private final class TankHandlerList extends HandlerList {
		public TankHandlerList() {
			setHandlers(new Handler[] {
					new TankWebSocketHandler(),
					new TankResourceHandler(),
					new DefaultHandler()});
		}
	}

	private final class TankWebSocketHandler extends WebSocketHandler {
		public WebSocket doWebSocketConnect(HttpServletRequest request, String arg1) {
			return new TankWebSocket();
		}
	}

	private final class TankResourceHandler extends ResourceHandler {
		public TankResourceHandler() {
                        setDirectoriesListed(true);
		//	setBaseResource(Resource.newClassPathResource(RESOURCE_PATH));
                        setResourceBase(".");
		}
	}

}
