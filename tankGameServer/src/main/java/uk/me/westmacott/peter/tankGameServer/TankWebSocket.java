package uk.me.westmacott.peter.tankGameServer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jetty.websocket.WebSocket;

public class TankWebSocket implements WebSocket.OnTextMessage {

	private static Set<TankWebSocket> OPEN_SOCKETS = new HashSet<TankWebSocket>();
	
	private static int nextId = 0;
	private static int getNextNumber() {
		return nextId++;
	}
	
	private final int id;
	private Connection connection;
	
	public TankWebSocket() {
		id = getNextNumber();
	}
	
	public void onClose(int arg0, String arg1) {
		System.out.println("Closing: " + id);
		OPEN_SOCKETS.remove(this);
		sendAll("D," + id);
	}

	public void onOpen(Connection newConnection) {
		System.out.println("Opening: " + id);
		connection = newConnection;
		OPEN_SOCKETS.add(this);
		
	}

	public void onMessage(String message) {
		if (message.startsWith("P")) {
			sendPoints(Integer.parseInt(message.split(",")[1]));
			return;
		}
		
		message = message + "," + id;
		sendAll(message);
	}

	private void sendPoints(Integer forId) {
		for (TankWebSocket socket : OPEN_SOCKETS) {
			if (forId.equals(socket.id)) { // could be optimised with a map, but this is an infrequent call.
				try {
					socket.connection.sendMessage("P");
					return;
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}
	
	private void sendAll(String message) {
		for (TankWebSocket each : OPEN_SOCKETS) {
			try {
				each.connection.sendMessage(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
