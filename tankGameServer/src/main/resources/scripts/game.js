
var SOUND;
var MUSIC;
var SOCKET;
var SHAPE;

require(["./socket", "./shape", "./sound", "./music"], function(socket, shape, sound, music) {
	SOCKET = socket;
	SHAPE = shape;
	SOUND = sound;
	MUSIC = music;
});

// tank.js
var TANK = {
		radius: 25
};
function Tank(text) {

	this.x = 0;
	this.y = 0;
	this.r = 0;
	this.turretR = 0;
	this.name = '';
	this.id = 0;

	if (text.length > 0) {
		bits = text.split(',');
		this.x = parseInt(bits[1]);
		this.y = parseInt(bits[2]);
		this.r = parseInt(bits[3]) / 100.0;
		this.turretR = parseInt(bits[4]) / 100.0;
		this.name = bits[5];
		this.id = parseInt(bits[6]);
	}

	this.forSocket = function () {
		return 'T,' + 
		Math.round(this.x) + ',' + 
		Math.round(this.y) + ',' + 
		Math.round(this.r * 100) + ',' + 
		Math.round(this.turretR * 100) + ',' +
		this.name; 
	};
}

// game.js
function Game() {
	
	var CONSTANTS = {
			width: 600,
			height: 600,
			originX: 300,
			originY: 500,
			tileCount: 20,
			tileSize: 256,
			health: 75
	};
	
	var LOCAL = new Tank('');
	var df = 0;
	var dr = 0;
	var ddf = 0;
	var health = 0;
	var kills = 0;
	var dies = 0;
	
	var resetting = 0;
	
	var gameCanvas = document.getElementById('gameCanvas');
	var context = gameCanvas.getContext("2d");
	var borderWidth = 5;
	var terrain = new Image();
	var body = new Image();
	var turret = new Image();
	var xMouse = 0;
	var yMouse = 0;
	var socket = SOCKET.create(getWSUrl());
	var explosions = [];
	var enemies = [];
	var lastExplosion = -1; // id of who caused the last explosion to damage your tank
	
	function getWSUrl() {
		var url = document.URL;
		var bits = url.split('/');
		return 'ws://' + bits[2] + '/tanks';
	}
	
	// Setup
	terrain.src = "./images/Dessert.png";
	body.src = "./images/TankBody.png";
	turret.src = "./images/TankTurret.png";
    
	gameCanvas.width = CONSTANTS.width;
	gameCanvas.height = CONSTANTS.height;
	context.translate(CONSTANTS.originX, CONSTANTS.originY);
	
	document.onmousemove = getMouseLocation;
	document.onkeydown = keyPressed;
	document.onkeyup = keyLifted;
	
	function reset() {
		var range = CONSTANTS.tileCount * CONSTANTS.tileSize;
		
		health = CONSTANTS.health;
		LOCAL.x = (Math.random() * range);
		LOCAL.y = (Math.random() * range);
		LOCAL.r = (Math.random() * 2.0 * Math.PI);
		LOCAL.turretR = 0;
	} 
	
	var firing = 0;
	var reload = 0;
	var maxReload = 15;
	
	// FIRE!!!
	gameCanvas.onmousedown = function(evt) {
		firing = 1;
		evt.preventDefault();
	};
	
	gameCanvas.onmouseup = function(evt) {
		firing = 0;
		evt.preventDefault();
	};
	
	function doFire() {
		
		if (reload > 0) {
			reload -= 1;
			return;
		}
		
		if (!firing) {
			return;
		}
		
		reload = maxReload;
		
		var xToOrigin = xMouse - gameCanvas.offsetLeft - CONSTANTS.originX - borderWidth;
		var yToOrigin = yMouse - gameCanvas.offsetTop - CONSTANTS.originY - borderWidth;
		var yRotated = Math.sin(LOCAL.r) * xToOrigin + Math.cos(LOCAL.r) * yToOrigin;
		var xRotated = Math.cos(LOCAL.r) * xToOrigin - Math.sin(LOCAL.r) * yToOrigin;
		var xAbsolute = xRotated + LOCAL.x;
		var yAbsolute = yRotated + LOCAL.y;
		
		var inter = SHAPE.intersect(LOCAL.x, LOCAL.y, xAbsolute, yAbsolute);
		if (inter) {
			xAbsolute = inter.x;
			yAbsolute = inter.y;
		}
		
		socket.send(
				'E,' + 
				xAbsolute + ',' + 
				yAbsolute + ',' + 
				LOCAL.x + ',' + 
				LOCAL.y 
		);
		
	};
	
	function getMouseLocation(event) {
		var ev = event || window.event;
		xMouse = ev.pageX;
		yMouse = ev.pageY;
	}
	
	function keyPressed(event) {
		/* 83 S, 68 D, 87 W, 65 A */
		if (event.which == 65) { dr = -0.06; return; }
		if (event.which == 68) { dr =  0.06; return; }
		if (event.which == 83) { ddf = -0.2; return; }
		if (event.which == 87) { ddf =  0.4; return; }
	}

	function keyLifted(event) {
		/* 83 S, 68 D, 87 W, 65 A */
		if (event.which == 65) { dr = 0; return; }
		if (event.which == 68) { dr = 0; return; }
		if (event.which == 83) { ddf = 0; return; }
		if (event.which == 87) { ddf = 0; return; }
	}
	
	function doRotate() {
		LOCAL.r += dr;
	}
	
	function doMove() {
		
		df += ddf;
		df *= 0.9;
		
		LOCAL.x += 2 * (df * Math.sin(LOCAL.r));
		LOCAL.y -= 2 * (df * Math.cos(LOCAL.r));

		var force = SHAPE.force(LOCAL.x, LOCAL.y);
		if (force) {
			LOCAL.x -= force.x;
			LOCAL.y -= force.y;
			df *= 0.9;
		}
		
		var min = 3 * CONSTANTS.tileSize;
		var max = (CONSTANTS.tileCount - 2) * CONSTANTS.tileSize;
		
		if (LOCAL.x < min + TANK.radius) { LOCAL.x = min + TANK.radius; }
		if (LOCAL.x > max - TANK.radius) { LOCAL.x = max - TANK.radius; }
		if (LOCAL.y < min + TANK.radius) { LOCAL.y = min + TANK.radius; }
		if (LOCAL.y > max - TANK.radius) { LOCAL.y = max - TANK.radius; }
		
	}
	
	
	function doHealthCheck() {
		if (health < 0) {
			dies++;
			socket.send('D');
			socket.send('T,-999999,-999999,0,0,Bob'); 
			socket.send('P,' + lastExplosion);
			resetting = 100;
		}
	}
	
	function renderTank(tank) {
		
		var halfImageSize = 24;
		
		var diffX = tank.x - LOCAL.x;
		var diffY = tank.y - LOCAL.y;
		
		var diffA = Math.atan2(diffY, diffX) + (Math.PI / 4.0);
		var diffH = Math.pow(diffX * diffX + diffY * diffY, 0.5);
		
		var diffXr = diffX * Math.cos(LOCAL.r) + diffY * Math.sin(LOCAL.r);
		var diffYr = diffX * Math.sin(LOCAL.r) - diffY * Math.cos(LOCAL.r);
		
		if (resetting && diffH < 2) {
			return;
		}
		
		if (	diffXr > (CONSTANTS.originX - CONSTANTS.width - TANK.radius) && 
				diffXr < (CONSTANTS.width - CONSTANTS.originX + TANK.radius) &&
				diffYr > (CONSTANTS.originY - CONSTANTS.height - TANK.radius) && 
				diffYr < (CONSTANTS.originY + TANK.radius)) { //diffH < CONSTANTS.width
			
			context.rotate(-LOCAL.r);
		
				context.translate(-LOCAL.x, -LOCAL.y);
				context.translate(tank.x, tank.y);
		
				context.rotate(tank.r);
					context.drawImage(body, -halfImageSize, -halfImageSize);
					context.rotate(tank.turretR);
						context.drawImage(turret, -halfImageSize, -halfImageSize);
					context.rotate(-tank.turretR);
				context.rotate(-tank.r);
		
				
				if (tank.name) {
					context.rotate(LOCAL.r);
					
					var textWidth = context.measureText(tank.name).width;
				
					context.globalAlpha = 0.5;
					context.beginPath();
					context.rect(20, 37, 4 + textWidth, -17);
					context.fillStyle = "rgb(50, 50, 50)";
					context.fill();
					context.globalAlpha = 1;
			    
					context.font = "15px Arial";
					context.fillStyle = "rgb(250, 250, 250)";
					context.fillText(tank.name, 22, 34);
				
					context.rotate(-LOCAL.r);
				}
				
				context.translate(-tank.x, -tank.y);
				context.translate(LOCAL.x, LOCAL.y);
				
			context.rotate(LOCAL.r);
		}
		else if (tank.x != -999999) {
			// Render indicator
			var indX = 50 * Math.cos(diffA) + 50 * Math.sin(diffA);
			var indY = 50 * Math.sin(diffA) - 50 * Math.cos(diffA);
			
			context.rotate(-LOCAL.r);
			context.fillStyle = 
				"rgb(150,250,150)";
			context.strokeStyle = 
				"rgb(0,50,0)";
			context.beginPath();
			context.arc(indX,
					indY,
					5,
					0,Math.PI*2,true);
			context.closePath();
			context.fill();
			context.stroke();
			context.rotate(LOCAL.r);
		}

		if (diffH > 2 && diffH < (2 * TANK.radius)) {
			// Repel
			LOCAL.x -= 4 * (Math.cos(diffA) + Math.sin(diffA));
			LOCAL.y -= 4 * (Math.sin(diffA) - Math.cos(diffA));
		}
	}
	
	function render() {
		var i = 0;
		var j = 0;

		var drawLimit = CONSTANTS.width + CONSTANTS.tileSize;
		
		var x = -LOCAL.x;
		var y = -LOCAL.y;

		LOCAL.name = document.getElementById('yourName').value;
		
		// Terrain
		context.rotate(-LOCAL.r);
		context.fillStyle = "#444444";
		for (i = 0; i <= CONSTANTS.tileCount; i++) {
			for (j = 0; j <= CONSTANTS.tileCount; j++) {
				if (Math.abs(x + (i * CONSTANTS.tileSize)) < drawLimit) {
					if (Math.abs(y + (j * CONSTANTS.tileSize)) < drawLimit) {

						if (i <= 2 || j <= 2 || i >= CONSTANTS.tileCount-2 || j >= CONSTANTS.tileCount-2) {
							if (i <= 0 || j <= 0 || i >= CONSTANTS.tileCount || j >= CONSTANTS.tileCount) {
								context.fillStyle = "#884444";
							}
							context.fillRect(x + (i * CONSTANTS.tileSize), y + (j * CONSTANTS.tileSize), CONSTANTS.tileSize, CONSTANTS.tileSize);
							context.fillStyle = "#444444";
						}
						else {
							context.drawImage(terrain, x + (i * CONSTANTS.tileSize), y + (j * CONSTANTS.tileSize));
						}
					}
				}
			}
		}
		context.rotate(LOCAL.r);

		// Shapes
		context.rotate(-LOCAL.r);
		context.translate(-LOCAL.x, -LOCAL.y);
		SHAPE.render(context);
		context.translate(LOCAL.x, LOCAL.y);
		context.rotate(LOCAL.r);
		
		// Tanks
		var angle = Math.atan2(yMouse - gameCanvas.offsetTop - CONSTANTS.originY - borderWidth, xMouse - gameCanvas.offsetLeft - CONSTANTS.originX - borderWidth);
		LOCAL.turretR = angle + (Math.PI / 2.0);
		for (var i in enemies) {
			if (enemies[i]) {
				renderTank(enemies[i]);
			}
		}
	
		// Explosions
		context.rotate(-LOCAL.r);
		gameCanvas.style.border = borderWidth + 'px double black';
		for (i = 0; i < explosions.length; i++) {
			
			var explosion = explosions[i];
			
			if (explosion.life > 0) {
				
				var xA = x + explosion.x; 
				var yA = y + explosion.y;
				var xB = x + explosion.fromX; 
				var yB = y + explosion.fromY;
				var size = 50 - Math.pow(explosion.life, 2) / 100;
				var p = SHAPE.newPoint(xA - xB, yA - yB);
				p.normalise();
				p.scale(30);
				var xC = xB + p.x;
				var yC = yB + p.y;
				
				if (xA * xA + yA * yA < size * size) {
					gameCanvas.style.border = borderWidth + 'px solid red';
					health--;
					lastExplosion = explosion.id;
				}
				
				var bright = 255 - (explosion.life * 5);
				context.fillStyle = "rgb(255," + bright + ",0)";
				context.globalAlpha = (explosion.life / 50.0);
				context.beginPath();
				context.arc(xA,
						yA,
						50 - Math.pow(explosion.life, 2) / 100,
						0,Math.PI*2,true);
				context.closePath();
				context.fill();
				
				bright = 255 - (explosion.life * 5);
				context.globalAlpha = (explosion.life / 100.0);
				context.fillStyle = "rgb(250,250,200)";
				context.beginPath();
				context.arc(xC, yC,
						10,
						0,Math.PI*2,true);
				context.closePath();
				context.fill();
				
				var grad = context.createLinearGradient(xA, yA, xB, yB);
				grad.addColorStop(0, "#FF0000");
				grad.addColorStop(0.5, "#000000");
				grad.addColorStop(1, "transparent");
				context.strokeStyle = grad;
				
				context.beginPath();
				context.moveTo(xA, yA);
				context.lineTo(xB, yB);
				context.stroke();

				explosion.life-=5;

			}	
		}
		if (explosions.length > 0 && explosions[0].life <= 0) {
			explosions.shift();
		}
		context.globalAlpha = 1.0;
		context.rotate(LOCAL.r);
		
		// HUD
		var barWidth = -10;
		var barInset = 10;
		
		context.beginPath();
	    context.rect(barInset - CONSTANTS.width/2, CONSTANTS.height - CONSTANTS.originY - barInset, Math.max(0, health), barWidth);
	    context.closePath();
	    context.fillStyle = "#44FF44";
	    context.fill();

	    context.beginPath();
	    context.rect(barInset - CONSTANTS.width/2, CONSTANTS.height - CONSTANTS.originY - barInset, CONSTANTS.health, barWidth);
	    context.closePath();
	    context.strokeStyle = "black";
	    context.stroke();
	    
	    var scoreText = 'Kills: ' + kills + '   Dies:' + dies;
	    var textWidth = context.measureText(scoreText).width;
	    
	    context.beginPath();
	    context.closePath();
	    context.rect(CONSTANTS.width/2 - barInset, CONSTANTS.height - CONSTANTS.originY - barInset, -4 - textWidth, -19);
	    context.fillStyle = "rgb(0, 0, 0)";
	    context.fill();
	 
	    context.font = "15px Arial";
		context.fillStyle = "rgb(250, 250, 250)";
		context.fillText(scoreText, CONSTANTS.width/2 - barInset - 2  -textWidth, CONSTANTS.height - CONSTANTS.originY - barInset - 4);
	    
	}
	
	function addExplosion(ex, ey, fx, fy, from) {
		explosions.push({
			life: 50, 
			x: ex, 
			y: ey,
			fromX: fx,
			fromY: fy,
			id: from
		});
	}
	
	function doReceive() {
		var msg;
		var bits;
		while (socket.canReceive()) {
			msg = socket.receive();
			if (msg.charAt(0) == 'E') {
				bits = msg.split(',');
				var xA = parseInt(bits[1]);
				var yA = parseInt(bits[2]);
				var xB = parseInt(bits[3]);
				var yB = parseInt(bits[4]);
				var from = parseInt(bits[5]);
				addExplosion(xA, yA, xB, yB, from);
				SOUND.play("bang", xA, yA, LOCAL.x, LOCAL.y);
			}
			else if (msg.charAt(0) == 'T') {
				var enemy = new Tank(msg);
				enemies[enemy.id] = enemy;
			}
			else if (msg.charAt(0) == 'D') {
				bits = msg.split(',');
				var enemy = enemies[bits[1]];
				if (enemy) {
					addExplosion(
							enemy.x, enemy.y,
							enemy.x, enemy.y, 
							bits[1]
					);
					//enemies[bits[1]] = null;
					enemies.splice(bits[1], bits[1]);
					SOUND.play("crash", enemy.x, enemy.y, LOCAL.x, LOCAL.y);
				}
			}
			else if (msg.charAt(0) == 'P') {
				kills++;
			}
		}
	}
	
	function doSend() {
		socket.send(LOCAL.forSocket());
	}
	
	reset();
	
	var resetter = 1;
	
	function doResetting() {
		resetting -= 1;
		if (resetting > 50 && (resetting%resetter == 0)) {
			
			resetter += 1;
			var ex = 5.0 - Math.random() * 10.0 + LOCAL.x;
			var ey = 5.0 - Math.random() * 10.0 + LOCAL.y;
			
			socket.send(
					'E,' + 
					ex + ',' + 
					ey + ',' + 
					ex + ',' + 
					ey
			);
		}
		if (!resetting) {
			resetter = 1;
			reset();
		}
	}
	
	setInterval(function () {
		
		doReceive();
		render();
		
		if (resetting) {
			doResetting();
		}
		else {
			doRotate();
			doMove();
			doSend();
			doFire();
			doHealthCheck();
		}
		
	}, 33);
	
}

var GAME = {
		begin: function() {
			document.getElementById('initialise').style.display = 'none';
			new Game();
		},
		toggleSound: function() {
			SOUND.toggle();
		},
		playMusic: function() {
			MUSIC.play();
		}
};