define(function () {

	function noop(){}
	
	function distance(x1, y1, x2, y2) {
		var xDiff = x1 - x2;
		var yDiff = y1 - y2;
		return Math.pow((xDiff * xDiff) + (yDiff * yDiff), 0.5);
	}

	function dotProduct(pointA, pointB) {
		return pointA.x * pointB.x + pointA.y * pointB.y;
	}

	function Point(x, y) {
		this.x = x;
		this.y = y;

		this.scale = function(s) {
			this.x *= s;
			this.y *= s;
		};

		this.normalise = function() {
			var length = this.magnitude();
			this.x /= length;
			this.y /= length;
		};

		this.magnitude = function() {
			return distance(0, 0, this.x, this.y);
		};

		this.toString = function() {
			return "(" + Math.round(this.x) + ", " + Math.round(this.y) + ")";
		};
	}

	function Shape() {

		this.points = [];
		this.colour = "#888888";

		this.render = function(context) {
			context.beginPath();
			context.moveTo(this.points[0].x, this.points[0].y);
			for (var i = 1; i < this.points.length; i++) {
				context.lineTo(this.points[i].x, this.points[i].y);
			}
			context.fillStyle = this.colour;
			context.fill();

		};

		// http://paulbourke.net/geometry/lineline2d/
		this.intersect = function(x1, y1, x2, y2) {

			var least = 1000000;
			var result = null;

			for (var i = 0; i < this.points.length; i++) {
				var pointA = this.points[i];
				var pointB = this.points[(i + 1)%this.points.length];

				var x3 = pointA.x;
				var y3 = pointA.y;
				var x4 = pointB.x;
				var y4 = pointB.y;

				var numerator1   = ((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3));
				var numerator2   = ((x2 - x1) * (y1 - y3)) - ((y2 - y1) * (x1 - x3));
				var denominator = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));

				if (denominator != 0) {

					var propOfInput = numerator1 / denominator;
					var propOfSide  = numerator2 / denominator;

					if (propOfInput > 0 && propOfInput < 1 && 
							propOfSide > 0 && propOfSide < 1) {

						var x5 = x1 + propOfInput * (x2 - x1);
						var y5 = y1 + propOfInput * (y2 - y1);

						if (propOfInput < least) {
							least = propOfInput;
							result = new Point(x5, y5);
						}


					}

				}

			}
			return result;
		};

		this.force = function(x, y) {

			var intersects = 1;
			var force = new Point(100000, 100000);
			var newForce;

			for (var i = 0; i < this.points.length; i++) {

				var pointA = this.points[i];
				var pointB = this.points[(i + 1)%this.points.length];
				var pointC = new Point(pointA.y - pointB.y, pointB.x - pointA.x); // normal from A
				var pointT = new Point(x - pointA.x, y - pointA.y); // T from A

				pointC.normalise();

				// D = project T onto A -> C

				var shared = dotProduct(pointC, pointT);
				var pointD = new Point(shared * pointC.x, shared * pointC.y);

				newForce = new Point(pointD.x, pointD.y);
				
				var signed = (shared > 0 ? 1 : -1);
				var adjusted = (((signed * newForce.magnitude()) - TANK.radius) * signed);
				newForce.normalise();
				newForce.scale(adjusted);
				if (newForce.magnitude() < force.magnitude()) {
					force = newForce;
				};

				if (adjusted * signed > 0) {	
					intersects = 0;
				}

			}
			if (intersects) {
				return force;
			}
			return;
		};
	}
	
	function createLowWall() {
		var shape = new Shape();
		shape.intersect = noop;
		shape.colour = "#666666";
		return shape;
	}
	
	function createHighWall() {
		var shape = new Shape();
		shape.force = noop;
		shape.colour = "#AAAAAA";
		return shape;
	}
	
	function createFullHeightWall() {
		var shape = new Shape();
		shape.colour = "#888888";
		return shape;
	}
	
	function createRectangle(creator, x1, y1, x2, y2) {
		var shape = creator();
		shape.points.push(new Point(x1, y1));
		shape.points.push(new Point(x1, y2));
		shape.points.push(new Point(x2, y2));
		shape.points.push(new Point(x2, y1));
		return shape;
	}
	
	function createWall(creator, x1, y1, x2, y2, width) {
		var shape = creator();
		
		var pointA = new Point(x1, y1);
		var pointB = new Point(x2, y2);
		var pointC = new Point(pointA.y - pointB.y, pointB.x - pointA.x);
		var pointD = new Point(pointA.x - pointB.x, pointA.y - pointB.y);
		
		pointC.normalise();
		pointD.normalise();
		pointC.scale(width);
		pointD.scale(width);
		
		shape.points.push(new Point(pointA.x - pointC.x, pointA.y - pointC.y));
		shape.points.push(new Point(pointA.x + pointD.x, pointA.y + pointD.y));
		shape.points.push(new Point(pointA.x + pointC.x, pointA.y + pointC.y));
		
		shape.points.push(new Point(pointB.x + pointC.x, pointB.y + pointC.y));
		shape.points.push(new Point(pointB.x - pointD.x, pointB.y - pointD.y));
		shape.points.push(new Point(pointB.x - pointC.x, pointB.y - pointC.y));
		
		return shape;
	}
	
	function createCastellatedWall(shapes, x1, y1, x2, y2, width) {
		
		shapes.push(createWall(createLowWall, x1, y1, x2, y2, width));
		
		var x3 = x1 * 0.25 + x2 * 0.75;
		var y3 = y1 * 0.25 + y2 * 0.75;
		var x4 = x2 * 0.25 + x1 * 0.75;
		var y4 = y2 * 0.25 + y1 * 0.75;
		
		shapes.push(createWall(createHighWall, x3, y3, x4, y4, width * 0.75));
		
	}
		
	shapes = [];
	
	var x, y, w, h;
	
//	x = 2560 - 256;
//	y = 2560 - 256;
//	w = 512;
//	h = 512;
//	shapes.push(createRectangle(createLowWall, x, y, x+w, y+h));
//	
//	x = 2560 - 200;
//	y = 2560 - 200;
//	w = 400;
//	h = 400;
//	shapes.push(createRectangle(createHighWall, x, y, x+w, y+h));
	
	var x1, y1, x2, y2;
	
	// L-shaped pieces
	w = 30;
	x1 = 2560 + 512;
	y1 = 2560 + 512 - w;
	x2 = x1;
	y2 = y1 - 256 - w;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);

	x1 = 2560 - 512;
	y1 = 2560 - 512 + w;
	x2 = x1;
	y2 = y1 + 256 + w;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);

	x1 = 2560 + 512 - w;
	y1 = 2560 + 512;
	x2 = x1 - 256 - w;
	y2 = y1;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	
	x1 = 2560 - 512 + w;
	y1 = 2560 - 512;
	x2 = x1 + 256 + w;
	y2 = y1;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	
	x1 = 2560 - 512;
	y1 = 2560 + 512 - w;
	x2 = x1;
	y2 = y1 - 256 - w;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	
	x1 = 2560 + 512;
	y1 = 2560 - 512 + w;
	x2 = x1;
	y2 = y1 + 256 + w;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	
	x1 = 2560 + 512 - w;
	y1 = 2560 - 512;
	x2 = x1 - 256 - w;
	y2 = y1;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	
	x1 = 2560 - 512 + w;
	y1 = 2560 + 512;
	x2 = x1 + 256 + w;
	y2 = y1;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	
	// box
	x1 = 2560 - 256;
	y1 = 2560 - 156;
	x2 = 2560 - 256;
	y2 = 2560 + 156;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	x1 = 2560 + 256;
	y1 = 2560 - 156;
	x2 = 2560 + 256;
	y2 = 2560 + 156;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	x1 = 2560 - 156;
	y1 = 2560 - 256;
	x2 = 2560 + 156;
	y2 = 2560 - 256;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	x1 = 2560 - 156;
	y1 = 2560 + 256;
	x2 = 2560 + 156;
	y2 = 2560 + 256;
	createCastellatedWall(shapes, x1, y1, x2, y2, w);
	
	
//	w = 30;
//	x1 = 2560 + 512;
//	y1 = 2560 + 512 - w;
//	x2 = x1;
//	y2 = y1 - 256;
//	shapes.push(createWall(createLowWall, x1, y1, x2, y2, w));
//	
//	w = 20;
//	x1 = 2560 + 512;
//	y1 = 2560 + 512 - w;
//	x2 = x1;
//	y2 = y1 - 128;
//	shapes.push(createWall(createHighWall, x1, y1, x2, y2, w));
//	
//	w = 30;
//	x1 = 2560 + 512 - w;
//	y1 = 2560 + 512;
//	x2 = x1 - 256;
//	y2 = y1;
//	shapes.push(createWall(createLowWall, x1, y1, x2, y2, w));
//	
//	w = 20;
//	x1 = 2560 + 512 - w;
//	y1 = 2560 + 512;
//	x2 = x1 - 128;
//	y2 = y1;
//	shapes.push(createWall(createHighWall, x1, y1, x2, y2, w));
	
	var shapeObject = {};
	
	shapeObject.render = function(context) {
		for (var i in shapes) {
			if (shapes[i]) {
				shapes[i].render(context);
			}
		}
	};
	
	shapeObject.force = function(x, y) {
		var total;
		var result;
		for (var i in shapes) {
			if (shapes[i]) {
				result = shapes[i].force(x, y);
				if (result) {
					if (!total) {
						total = new Point(0, 0);
					}
					total.x += result.x;
					total.y += result.y;
				}
			}
		}
		return total;
	};

	shapeObject.intersect = function(x1, y1, x2, y2) {
		var result;
		for (var i in shapes) {
			if (shapes[i]) {
				result = shapes[i].intersect(x1, y1, x2, y2);
				if (result) {
					return result;
				}
			}
		}
		return;
	};
	
	shapeObject.newPoint = function(x, y) {
		return new Point(x,y);
	};

	return shapeObject;
});
