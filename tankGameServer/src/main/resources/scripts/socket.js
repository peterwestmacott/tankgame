define(function () {
		
	if (window.MozWebSocket) {
		window.WebSocket = window.MozWebSocket;
	}
	
	function Socket(target) {
		
		var socket = null;
		var incoming = [];
		var unsent = [];
		
		checkSocket();
		
		function checkSocket() {
			if (socket == null) {
				socket = new WebSocket(target);
				socket.onmessage = function (event) {
					incoming.push(event.data);
				};
				socket.onclose = function (event) {
					socket = null;
				};
			}
		}

		this.send = function (message) {
			unsent.push(message);
			var nextMsg;
			try {
				while (unsent.length) {
					nextMsg = unsent.shift();
					socket.send(nextMsg);
				}
			}
			catch(err) {
				checkSocket();
				unsent.push(nextMsg);
			}
		};
		
		this.canReceive = function () {
			return incoming.length;
		};
		
		this.receive = function () {
			return incoming.shift();
		};
		
	}
	
	var socketObject = {};
	socketObject.create = function(target) {
		return new Socket(target);
	};
	
	return socketObject;
	
});