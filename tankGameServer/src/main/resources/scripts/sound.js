define(function () {

	var soundObject = {};
	var sound = true;
	var soundElement = document.getElementById('sound');

	soundObject.toggle = function() {
		sound = !sound;
		if (sound) {
			soundElement.innerHTML = 'Sound = ON';
		} 
		else {
			soundElement.innerHTML = 'Sound = OFF';
		}
	};

	soundObject.play = function(name, x1, y1, x2, y2) {
		if (sound) {
			var snd = new Audio("../sounds/" + name + ".wav");
			var xDiff = x1 - x2;
			var yDiff = y1 - y2;
			var distanceSquared = (xDiff * xDiff) + (yDiff * yDiff);
			snd.volume = Math.min(1.0, 1000000.0 / distanceSquared);
			snd.play();
		}
	};
	
	return soundObject;
});