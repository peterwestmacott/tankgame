
var context;

function debugPoint(x, y) {
	
	context.fillStyle = 'FF0000';
	
	context.beginPath();
	context.moveTo(x, y);
	context.lineTo(x, y + 15);
	context.lineTo(x + 15, y);
	context.lineTo(x, y);
	context.closePath();
	
	context.fill();
	
	context.beginPath();
	context.moveTo(x, y);
	context.lineTo(x, y - 15);
	context.lineTo(x - 15, y);
	context.lineTo(x, y);
	context.closePath();
	
	context.fill();
	
	
}


function debug(msg) {
	document.getElementById('debug').innerHTML = msg;
}