
var sound = 1;

function toggleSound() {
	sound = 1-sound;
	
	if (sound) {
		document.getElementById('sound').innerHTML = 'Sound = ON';
	} 
	else {
		document.getElementById('sound').innerHTML = 'Sound = OFF';
	}
}


function Game() {

	var GAME = {
			width: 600,
			height: 600,
			originX: 300,
			originY: 500,
			tileCount: 10,
			tileSize: 256,
			health: 75
	}
	
	var LOCAL = new Tank('');
	var df = 0;
	var dr = 0;
	var ddf = 0;
	var ddr = 0;
	var health = 0;
	var kills = 0;
	var dies = 0;
	
	var resetting = 0;
	
	var gameCanvas = document.getElementById('gameCanvas');
	context = gameCanvas.getContext("2d");
	var borderWidth = 5;
	var terrain = new Image();
	var body = new Image();
	var turret = new Image();
	var xMouse = 0;
	var yMouse = 0;
	var socket = new Socket(getWSUrl());
	var explosions = [];
	var enemies = [];
		
	function getWSUrl() {
		var url = document.URL;
		var bits = url.split('/');
		return 'ws://' + bits[2] + '/tanks';
	}
	
	function getDistance(x1, y1, x2, y2) {
		var xDiff = x1 - x2;
		var yDiff = y1 - y2;
		return Math.pow((xDiff * xDiff) + (yDiff * yDiff), 0.5);
	}
	
	// Setup
	terrain.src = "dessert.png";
	body.src = "TankBody3.png";
	turret.src = "TankTurret3.png";
    
	gameCanvas.width = GAME.width;
	gameCanvas.height = GAME.height;
	context.translate(GAME.originX, GAME.originY);
	
	document.onmousemove = getMouseLocation;
	document.onkeydown = keyPressed;
	document.onkeyup = keyLifted;
	
	function reset() {
		var range = GAME.tileCount * GAME.tileSize;
		
		health = GAME.health;
		LOCAL.x = (Math.random() * range);
		LOCAL.y = (Math.random() * range);
		LOCAL.r = (Math.random() * 2.0 * Math.PI);
		LOCAL.turretR = 0;
	} 
	
//	var firing;
	var reload;
	var maxReload = 20;
	
	// FIRE!!!
//	gameCanvas.onmousedown = function() {
//		firing = 1;
//	}
//	
//	gameCanvas.onmouseup = function() {
//		firing = 0;
//	}
	
	gameCanvas.onmousedown = function() {
		
		if (reload > 0) {
			return;
		}
		
		reload = maxReload;
		
		var xToOrigin = xMouse - gameCanvas.offsetLeft - GAME.originX - borderWidth;
		var yToOrigin = yMouse - gameCanvas.offsetTop - GAME.originY - borderWidth;
		var yRotated = Math.sin(LOCAL.r) * xToOrigin + Math.cos(LOCAL.r) * yToOrigin;
		var xRotated = Math.cos(LOCAL.r) * xToOrigin - Math.sin(LOCAL.r) * yToOrigin;
		var xAbsolute = xRotated + LOCAL.x;
		var yAbsolute = yRotated + LOCAL.y;
		
		
		var shape = new Shape();
		var inter = shape.intersect(LOCAL.x, LOCAL.y, xAbsolute, yAbsolute);
		if (inter) {
			xAbsolute = inter.x;
			yAbsolute = inter.y;
		}
		
		socket.send(
				'E,' + 
				xAbsolute + ',' + 
				yAbsolute + ',' + 
				LOCAL.x + ',' + 
				LOCAL.y 
		);
	}
	
	function doFire() {
	
		reload -= 1;
		
		var x = xMouse - gameCanvas.offsetLeft - GAME.originX - borderWidth;
		var y = yMouse - gameCanvas.offsetTop - GAME.originY - borderWidth;
		
		context.globalAlpha = 0.3;
		if (reload > 0) {
			context.beginPath();
			context.moveTo(x, y);
			context.arc(
					x,
					y,
					15,
					-0.5 * Math.PI,
					1.5 * Math.PI - Math.PI * 2 * (reload/maxReload), 
					true);
			context.lineTo(x, y);
			context.closePath();
			context.fillStyle = '#FF0000';
			context.fill();
		}
		else {
			context.beginPath();
			context.arc(
					x,
					y,
					15,
					0,
					2 * Math.PI, 
					true);
			context.closePath();
			context.fillStyle = 'rgb(0, 255, 0)';
			context.fill();
		}
		context.globalAlpha = 1.0;
		
		context.strokeStyle = '#000000';
		context.beginPath();
		context.arc(
				x,
				y,
				15,
				0, Math.PI * 2, true);
		context.moveTo(x - 15, y);
		context.lineTo(x + 15, y);
		context.moveTo(x, y - 15);
		context.lineTo(x, y + 15);
		context.closePath();
		context.stroke();
		
	}
	
	function getMouseLocation(event) {
		var ev = event || window.event;
		xMouse = ev.pageX;
		yMouse = ev.pageY;
	}
	
	function keyPressed(event) {
		/* 83 S, 68 D, 87 W, 65 A */
		if (event.which == 65) { dr = -0.06; return; }
		if (event.which == 68) { dr =  0.06; return; }
		if (event.which == 83) { ddf = -0.2; return; }
		if (event.which == 87) { ddf =  0.4; return; }
	}

	function keyLifted(event) {
		/* 83 S, 68 D, 87 W, 65 A */
		if (event.which == 65) { dr = 0; return; }
		if (event.which == 68) { dr = 0; return; }
		if (event.which == 83) { ddf = 0; return; }
		if (event.which == 87) { ddf = 0; return; }
	}
	
	function doRotate() {
		LOCAL.r += dr;
	}
	
	function doMove() {
		
		df += ddf;
		df *= 0.9;
		
		LOCAL.x += 2 * (df * Math.sin(LOCAL.r));
		LOCAL.y -= 2 * (df * Math.cos(LOCAL.r));

		var force = new Shape().force(LOCAL.x, LOCAL.y);
		if (force) {
			LOCAL.x -= force.x;
			LOCAL.y -= force.y;
			df *= 0.9;
		}
		
		var min = 3 * GAME.tileSize;
		var max = (GAME.tileCount - 2) * GAME.tileSize;
		
		if (LOCAL.x < min + TANK.radius) { LOCAL.x = min + TANK.radius; }
		if (LOCAL.x > max - TANK.radius) { LOCAL.x = max - TANK.radius; }
		if (LOCAL.y < min + TANK.radius) { LOCAL.y = min + TANK.radius; }
		if (LOCAL.y > max - TANK.radius) { LOCAL.y = max - TANK.radius; }
		
	}
	
	var lastExplosion; // id of who caused the last explosion to damage your tank
	
	function doHealthCheck() {
		if (health < 0) {
			dies++;
			socket.send('D');
			socket.send('T,-999999,-999999,0,0,Bob'); 
			socket.send('P,' + lastExplosion);
			resetting = 100;
		}
	}
	
	function renderTank(tank) {
		
		var halfImageSize = 24;
		
		var diffX = tank.x - LOCAL.x;
		var diffY = tank.y - LOCAL.y;
		
		var diffA = Math.atan2(diffY, diffX) + (Math.PI / 4.0);
		var diffH = Math.pow(diffX * diffX + diffY * diffY, 0.5);
		
		var diffXr = diffX * Math.cos(LOCAL.r) + diffY * Math.sin(LOCAL.r);
		var diffYr = diffX * Math.sin(LOCAL.r) - diffY * Math.cos(LOCAL.r);
		
		if (resetting && diffH < 2) {
			return;
		}
		
		if (	diffXr > (GAME.originX - GAME.width - TANK.radius) && 
				diffXr < (GAME.width - GAME.originX + TANK.radius) &&
				diffYr > (GAME.originY - GAME.height - TANK.radius) && 
				diffYr < (GAME.originY + TANK.radius)) { //diffH < GAME.width
			
			context.rotate(-LOCAL.r);
		
				context.translate(-LOCAL.x, -LOCAL.y);
				context.translate(tank.x, tank.y);
		
				context.rotate(tank.r);
					context.drawImage(body, -halfImageSize, -halfImageSize);
					context.rotate(tank.turretR);
						context.drawImage(turret, -halfImageSize, -halfImageSize);
					context.rotate(-tank.turretR);
				context.rotate(-tank.r);
		
				
				if (tank.name) {
					context.rotate(LOCAL.r);
					
					var textWidth = context.measureText(tank.name).width;
				
					context.globalAlpha = 0.5;
					context.beginPath();
					context.rect(20, 37, 4 + textWidth, -17);
					context.fillStyle = "rgb(50, 50, 50)";
					context.fill();
					context.globalAlpha = 1;
			    
					context.font = "15px Arial";
					context.fillStyle = "rgb(250, 250, 250)";
					context.fillText(tank.name, 22, 34);
				
					context.rotate(-LOCAL.r);
				}
				
				context.translate(-tank.x, -tank.y);
				context.translate(LOCAL.x, LOCAL.y);
				
			context.rotate(LOCAL.r);
		}
		else if (tank.x != -999999) {
			// Render indicator
			var indX = 50 * Math.cos(diffA) + 50 * Math.sin(diffA);
			var indY = 50 * Math.sin(diffA) - 50 * Math.cos(diffA);
			
			context.rotate(-LOCAL.r);
			context.fillStyle = 
				"rgb(150,250,150)";
			context.strokeStyle = 
				"rgb(0,50,0)";
			context.beginPath();
			context.arc(indX,
					indY,
					5,
					0,Math.PI*2,true);
			context.closePath();
			context.fill();
			context.stroke();
			context.rotate(LOCAL.r);
		}

		if (diffH > 2 && diffH < (2 * TANK.radius)) {
			// Repel
			LOCAL.x -= 4 * (Math.cos(diffA) + Math.sin(diffA));
			LOCAL.y -= 4 * (Math.sin(diffA) - Math.cos(diffA));
		}
	}
	
	function render() {
		var i = 0;
		var j = 0;
		var k, l;

		var drawLimit = GAME.width + GAME.tileSize;
		
		var x = -LOCAL.x;
		var y = -LOCAL.y;
		var r = LOCAL.r;

		LOCAL.name = document.getElementById('yourName').value;
		
		// Terrain
		context.rotate(-LOCAL.r);
		context.fillStyle = "#444444";
		for (i = 0; i <= GAME.tileCount; i++) {
			for (j = 0; j <= GAME.tileCount; j++) {
				if (Math.abs(x + (i * GAME.tileSize)) < drawLimit) {
					if (Math.abs(y + (j * GAME.tileSize)) < drawLimit) {

						if (i <= 2 || j <= 2 || i >= GAME.tileCount-2 || j >= GAME.tileCount-2) {
							if (i <= 0 || j <= 0 || i >= GAME.tileCount || j >= GAME.tileCount) {
								context.fillStyle = "#884444";
							}
							context.fillRect(x + (i * GAME.tileSize), y + (j * GAME.tileSize), GAME.tileSize, GAME.tileSize);
							context.fillStyle = "#444444";
						}
						else {
							context.drawImage(terrain, x + (i * GAME.tileSize), y + (j * GAME.tileSize));
						}
					}
				}
			}
		}
		context.rotate(LOCAL.r);
		
		
//		var force = shape.force(LOCAL.x, LOCAL.y);
//		if (force) {
//			LOCAL.x -= force.x / 2;
//			LOCAL.y -= force.y / 2;
//		}

		// Tanks
		var angle = Math.atan2(yMouse - gameCanvas.offsetTop - GAME.originY - borderWidth, xMouse - gameCanvas.offsetLeft - GAME.originX - borderWidth);
		LOCAL.turretR = angle + (Math.PI / 2.0);
		for (var i in enemies) {
			if (enemies[i]) {
				renderTank(enemies[i]);
			}
		}
	
		// Explosions
		context.rotate(-LOCAL.r);
		gameCanvas.style.border = borderWidth + 'px double black';
		for (i = 0; i < explosions.length; i++) {
			
			var explosion = explosions[i];
			
			if (explosion.life > 0) {
				
				var xA = x + explosion.x; 
				var yA = y + explosion.y;
				var xB = x + explosion.fromX; 
				var yB = y + explosion.fromY;
				var size = 50 - Math.pow(explosion.life, 2) / 100
				
				if (xA * xA + yA * yA < size * size) {
					gameCanvas.style.border = borderWidth + 'px solid red';
					health--;
					//document.getElementById('debug').innerHTML = '('+Math.round(health)+')';
					lastExplosion = explosion.id;
				}
				
				var bright = 255 - (explosion.life * 5);
				context.fillStyle = "rgb(255," + bright + ",0)"
				context.globalAlpha = (explosion.life / 50.0);
				context.beginPath();
				context.arc(xA,
						yA,
						50 - Math.pow(explosion.life, 2) / 100,
						0,Math.PI*2,true);
				context.closePath();
				context.fill();
				
				var grad = context.createLinearGradient(xA, yA, xB, yB);
				
				grad.addColorStop(0, "#FF0000");
				grad.addColorStop(0.5, "#FFFF00");
				grad.addColorStop(1, "transparent");
				context.strokeStyle = grad;
				
				context.beginPath();
				context.moveTo(xA, yA);
				context.lineTo(xB, yB);
				context.stroke();

				explosion.life-=5;

			}	
		}
		if (explosions.length > 0 && explosions[0].life <= 0) {
			explosions.shift();
		}
		context.globalAlpha = 1.0;
		context.rotate(LOCAL.r);
		
		context.rotate(-LOCAL.r);
		context.translate(-LOCAL.x, -LOCAL.y);
		var shape = new Shape();
		shape.render(context);
		context.translate(LOCAL.x, LOCAL.y);
		context.rotate(LOCAL.r);
		
		// HUD
		
		var barWidth = -10;
		var barInset = 10;
		
		context.beginPath();
	    context.rect(barInset - GAME.width/2, GAME.height - GAME.originY - barInset, Math.max(0, health), barWidth);
	    context.closePath();
	    context.fillStyle = "#44FF44";
	    context.fill();

	    context.beginPath();
	    context.rect(barInset - GAME.width/2, GAME.height - GAME.originY - barInset, GAME.health, barWidth);
	    context.closePath();
	    context.strokeStyle = "black";
	    context.stroke();
	    
	    var scoreText = 'Kills: ' + kills + '   Dies:' + dies;
	    var textWidth = context.measureText(scoreText).width;
	    
	    context.beginPath();
	    context.closePath();
	    context.rect(GAME.width/2 - barInset, GAME.height - GAME.originY - barInset, -4 - textWidth, -19);
	    context.fillStyle = "rgb(0, 0, 0)";
	    context.fill();
	 
	    context.font = "15px Arial";
		context.fillStyle = "rgb(250, 250, 250)";
		context.fillText(scoreText, GAME.width/2 - barInset - 2  -textWidth, GAME.height - GAME.originY - barInset - 4);
	    
	}
	
	function addExplosion(ex, ey, fx, fy, from) {
		explosions.push({
			life: 50, 
			x: ex, 
			y: ey,
			fromX: fx,
			fromY: fy,
			id: from
		});
	}
	
	function doReceive() {
		var msg;
		var bits;
		while (socket.canReceive()) {
			msg = socket.receive();
			if (msg.charAt(0) == 'E') {
				bits = msg.split(',');
				var xA = parseInt(bits[1]);
				var yA = parseInt(bits[2]);
				var xB = parseInt(bits[3]);
				var yB = parseInt(bits[4]);
				var from = parseInt(bits[5]);
				addExplosion(xA, yA, xB, yB, from);
				doSound("bang4", getDistance(xA, yA, LOCAL.x, LOCAL.y));
			}
			else if (msg.charAt(0) == 'T') {
				var enemy = new Tank(msg);
				enemies[enemy.id] = enemy;
			}
			else if (msg.charAt(0) == 'D') {
				bits = msg.split(',');
				var enemy = enemies[bits[1]];
				if (enemy) {
					addExplosion(
							enemy.x, enemy.y,
							enemy.x, enemy.y, 
							bits[1]
					);
					enemies[bits[1]] = null;
					doSound("crash", getDistance(enemy.x, enemy.y, LOCAL.x, LOCAL.y));
				}
			}
			else if (msg.charAt(0) == 'P') {
				kills++;
			}
		}
	}
	
	function doSound(name, distance) {
		if (sound) {
			var snd = new Audio(name + ".wav");
			snd.volume = Math.min(1.0, 1000000.0 / (distance * distance));
			snd.play();
		}
	}
	
	function doSend() {
		socket.send(LOCAL.forSocket());
	}
	
	reset();
	
	var resetter = 1;
	
	function doResetting() {
		resetting -= 1;
		if (resetting > 50 && (resetting%resetter == 0)) {
			
			resetter += 1;
			var ex = 5.0 - Math.random() * 10.0 + LOCAL.x;
			var ey = 5.0 - Math.random() * 10.0 + LOCAL.y;
			
			socket.send(
					'E,' + 
					ex + ',' + 
					ey + ',' + 
					ex + ',' + 
					ey
			);
		}
		if (!resetting) {
			resetter = 1;
			reset();
		}
	}
	
	setInterval(function () {
		
		doReceive();
		render();
		
		if (resetting) {
			doResetting();
		}
		else {
			doRotate();
			doMove();
			doSend();
			doFire();
			doHealthCheck();
		}
		
	}, 33);
	
}