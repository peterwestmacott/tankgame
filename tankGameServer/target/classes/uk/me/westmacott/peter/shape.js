
function distance(x1, y1, x2, y2) {
	var xDiff = x1 - x2;
	var yDiff = y1 - y2;
	return Math.pow((xDiff * xDiff) + (yDiff * yDiff), 0.5);
}

function Point(x, y) {
	this.x = x;
	this.y = y;
	
	this.scale = function(s) {
		this.x *= s;
		this.y *= s;
	}
	
	this.normalise = function() {
		var length = this.magnitude();
		this.x /= length;
		this.y /= length;
	}
	
	this.magnitude = function() {
		return distance(0, 0, this.x, this.y);
	}
	
	this.toString = function() {
		return "(" + Math.round(this.x) + ", " + Math.round(this.y) + ")";
	}
}


function dotProduct(pointA, pointB) {
	return pointA.x * pointB.x + pointA.y * pointB.y;
}


function Shape() {
	
	
	
	var points = [];
	
	points.push(new Point(1200, 1300));
	points.push(new Point(1256, 1412));
	points.push(new Point(1512, 1512));
	points.push(new Point(1612, 1256));
	
	
	this.render = function(context) {
		context.beginPath();
		context.moveTo(points[0].x, points[0].y);
		for (var i = 1; i < points.length; i++) {
			context.lineTo(points[i].x, points[i].y);
		}
		context.fillStyle = "#888888";
		context.fill();
		
	}
	
	// http://paulbourke.net/geometry/lineline2d/
	this.intersect = function(x1, y1, x2, y2) {
		
		var least = 1000000;
		var result;
		
		for (var i = 0; i < points.length; i++) {
			var pointA = points[i];
			var pointB = points[(i + 1)%points.length];
			
			var x3 = pointA.x;
			var y3 = pointA.y;
			var x4 = pointB.x;
			var y4 = pointB.y;
			
			var numerator1   = ((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3));
			var numerator2   = ((x2 - x1) * (y1 - y3)) - ((y2 - y1) * (x1 - x3));
			var denominator = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
			
			if (denominator != 0) {
			
				var propOfInput = numerator1 / denominator;
				var propOfSide  = numerator2 / denominator;
				
				if (propOfInput > 0 && propOfInput < 1 && 
				    propOfSide > 0 && propOfSide < 1) {
					
					var x5 = x1 + propOfInput * (x2 - x1);
					var y5 = y1 + propOfInput * (y2 - y1);
					
					if (propOfInput < least) {
						least = propOfInput;
						result = new Point(x5, y5);
					}
					
					
				}
			
			}
			
		}
		return result;
	}
	
	this.force = function(x, y) {
		
		var intersects = 1;
		var force = new Point(100000, 100000);
		var newForce;
		
		for (var i = 0; i < points.length; i++) {
		
			var pointA = points[i];
			var pointB = points[(i + 1)%points.length];
			var pointC = new Point(pointA.y - pointB.y, pointB.x - pointA.x); // normal from A
			var pointT = new Point(x - pointA.x, y - pointA.y); // T from A
			
			pointC.normalise();
			
			// D = project T onto A -> C
			
			var shared = dotProduct(pointC, pointT);
			var pointD = new Point(shared * pointC.x, shared * pointC.y);
			
			var pointG = new Point(pointA.x + pointD.x, pointA.y + pointD.y);
			var signed = (shared > 0 ? 1 : -1);
			
			newForce = new Point(pointD.x, pointD.y);
			var adjusted = (((signed * newForce.magnitude()) - TANK.radius) * signed);
			newForce.normalise();
			newForce.scale(adjusted);
			if (newForce.magnitude() < force.magnitude()) {
				force = newForce;
			};
			
			//if ((signed * pointD.magnitude()) - TANK.radius > 0) {
			if (adjusted * signed > 0) {	
				intersects = 0;
			}
			
//			context.beginPath();
//			context.moveTo(pointA.x, pointA.y);
//			context.lineTo(pointA.x + pointC.x, pointA.y + pointC.y);
//			context.closePath();
//			context.strokeStyle = "#00FF00";
//			context.stroke();
//			
//			context.beginPath();
//			context.moveTo(pointA.x, pointA.y);
//			context.lineTo(pointA.x + pointT.x, pointA.y + pointT.y);
//			context.closePath();
//			context.strokeStyle = "#00FF00";
//			context.stroke();
//			
//			context.beginPath();
//			context.moveTo(pointA.x, pointA.y);
//			context.lineTo(pointG.x, pointG.y);
//			context.closePath();
//			context.strokeStyle = "#00FFFF";
//			context.stroke();
//			
//			context.fillStyle = "#0000FF";
//			context.beginPath();
//			context.arc(pointG.x,
//					pointG.y,
//					5,
//					0,Math.PI*2,true);
//			context.closePath();
//			context.fill();
			
			// measure distance;
			
		}
		
//		context.fillStyle = "#00FF00";
//		if (intersects) {
//			context.fillStyle = "#0000FF";
//		}
//		
//		context.beginPath();
//		context.arc(x,
//				y,
//				TANK.radius,
//				0,Math.PI*2,true);
//		context.closePath();
//		context.fill();
		
		if (intersects) {
//			context.beginPath();
//			context.moveTo(x, y);
//			context.lineTo(x + 5*force.x, y + 5*force.y);
//			context.closePath();
//			context.strokeStyle = "#000000";
//			context.stroke();
			return force;
		}
		return;
		
	}
	
	
}
