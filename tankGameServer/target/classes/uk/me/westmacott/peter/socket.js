


function Socket(target) {
	
	var socket;
	var incoming = [];
	var that = this;
	
	checkSocket();
	
	function checkSocket() {
		if (socket == null) {
			socket = new WebSocket(target);
			socket.onmessage = function (event) {
				incoming.push(event.data);
			}
			socket.onclose = function (event) {
				socket = null;
			}
		}
	}
	
	this.send = function (message) {
		checkSocket(); // I'm not sure that this works.
		socket.send(message);
	}
	
	this.canReceive = function () {
		return incoming.length;
	}
	
	this.receive = function () {
		return incoming.shift();
	}
	
}