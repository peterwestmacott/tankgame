

var TANK = {
		radius: 25
}


function Tank(text) {

	this.x = 0;
	this.y = 0;
	this.r = 0;
	this.turretR = 0;
	this.name = '';
	this.id = 0;

	if (text.length > 0) {
		bits = text.split(',');
		this.x = parseInt(bits[1]);
		this.y = parseInt(bits[2]);
		this.r = parseInt(bits[3]) / 100.0;
		this.turretR = parseInt(bits[4]) / 100.0;
		this.name = bits[5];
		this.id = parseInt(bits[6]);
	}

	this.forSocket = function () {
		return 'T,' + 
		Math.round(this.x) + ',' + 
		Math.round(this.y) + ',' + 
		Math.round(this.r * 100) + ',' + 
		Math.round(this.turretR * 100) + ',' +
		this.name; 
	}
}


